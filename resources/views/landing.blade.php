<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Ben</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700%7CMontserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
  </head>

  <body>

    <div id="h">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-md-10 centered">
            <img src="assets/img/logo22.png" width="70%">
            <h2 class="thin ">نظام مراقبة استهلاك الوقود<br></h2><h3 class="thin "> <red> Built with 🖤 at <a class="bold" style="color: #e8330f" href="https://www.facebook.com/events/333690580368261/">Benghazi hackthon 2018</a></red><br>  - Alpha Team - </h3>

            <h4> Fares ALmajbri
            - Nabil Kechroud - Salem USB</h4>


          
            <span><a href="/admin" class="btn btn-danger btn-lg  btn-lg mt-50" ><h4 style="color: white">تطبيق الويب</h4></a></span>
            <span><a class="btn btn-warning btn-lg  btn-lg mt-50" href="benzintec.apk"><h4 >تطبيق الهاتف</h4></a></span>
            
          </div>
            <img src="assets/img/banner.png"  class="mt-50">

        </div>
      </div>
    </div><!--/Header-->

       






    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
