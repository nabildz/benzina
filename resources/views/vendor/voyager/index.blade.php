@extends('voyager::master')


@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
          احصائيات {{ Auth::user()->name }} 
        </h1>
       
    </div>
@stop

@section('content')
<style>
    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    </style>
 <div class="page-content browse container">

    @if(Auth::user()->role_id == 1 && Auth::user()->roles == '[]')
<div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body panel-body-accent">
                            <div class="media no-margin">
                                <div class="media-left media-middle">
                                    <i class="far fa-credit-card icon-3x text-success-400"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="no-margin text-semibold">1700</h3>
                                    <span class="text-uppercase text-size-mini text-muted">عدد الطلبيات</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body">
                            <div class="media no-margin">
                                <div class="media-left media-middle">
                                    <i class="fas fa-car icon-3x text-indigo-400"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="no-margin text-semibold">1700</h3>
                                    <span class="text-uppercase text-size-mini text-muted">عدد المراكب المسجلة</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body">
                            <div class="media no-margin">
                                <div class="media-body">
                                    <h3 class="no-margin text-semibold">1000</h3>
                                    <span class="text-uppercase text-size-mini text-muted">عدد السائقيين</span>
                                </div>

                                <div class="media-right media-middle">
                                    <i class="fa fa-users icon-3x text-blue-400"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body">
                            <div class="media no-margin">
                                <div class="media-body">
                                    <h3 class="no-margin text-semibold">465</h3>
                                    <span class="text-uppercase text-size-mini text-muted">محطات الوقود</span>
                                </div>

                                <div class="media-right media-middle">
                                    <i class="fas fa-gas-pump icon-3x text-danger-400"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-flat">
                    <div class="panel-heading">
                          <h1 class="page-title">نسبة استهلاك الوقود الى الوقود المباع</h1>
                        
                    </div>

                    <div class="panel-body">
                        <div class="chart-container">
                          <canvas id="myChart" width="400" height="200"></canvas>
    <script>
        var lineChartData = {
            labels: ['أبريل', 'مايو', 'يونيو','أبريل', 'مايو', 'يونيو' ],
            datasets: [{
                label: 'كمية الوقود المباع',
                borderColor: window.chartColors.red,
                backgroundColor: window.chartColors.red,
                fill: false,
                data: [
                    160,
                    170,
                    160,
                    150,
                    160,
                    170

                ],
                yAxisID: 'y-axis-1',
            }, {
                label: 'كمية الوقود المستهلك',
                borderColor: window.chartColors.blue,
                backgroundColor: window.chartColors.blue,
                fill: false,
                data: [
                    150,
                    160,
                    140,
                    130,
                    150,
                    170
                ],
                yAxisID: 'y-axis-2'
            }]
        };

        window.onload = function() {
            var ctx = document.getElementById('myChart').getContext('2d');
            window.myLine = Chart.Line(ctx, {
                data: lineChartData,
                options: {
                    responsive: true,
                    hoverMode: 'index',
                    stacked: false,
                    title: {
                        display: false,
                        text: 'Chart.js Line Chart - Multi Axis'
                    },
                    scales: {
                        yAxes: [{
                            type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: true,
                            position: 'left',
                            id: 'y-axis-1',
                        }, {
                            type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: true,
                            position: 'right',
                            id: 'y-axis-2',

                            // grid line settings
                            gridLines: {
                                drawOnChartArea: false, // only want the grid lines for one axis to show up
                            },
                        }],
                    }
                }
            });
        };

        document.getElementById('randomizeData').addEventListener('click', function() {
            lineChartData.datasets.forEach(function(dataset) {
                dataset.data = dataset.data.map(function() {
                    return randomScalingFactor();
                });
            });

            window.myLine.update();
        });
    </script>
                        </div>
                    </div>
                </div>

                </div>




    @elseif(Auth::user()->role_id == 1 &&  Auth::user()->roles->contains('3') )


    <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="panel panel-body panel-body-accent">
                            <div class="media no-margin">
                                <div class="media-left media-middle">
                                    <i class="fas fa-clipboard-list icon-3x text-success-400"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="no-margin text-semibold">65</h3>
                                    <span class="text-uppercase text-size-mini text-muted">عدد الطلبيات المنجزة</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4">
                        <div class="panel panel-body">
                            <div class="media no-margin">
                                <div class="media-left media-middle">
                                       <i class="fas fa-clipboard-list icon-3x text-danger-400"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="no-margin text-semibold">103</h3>
                                    <span class="text-uppercase text-size-mini text-muted">عدد الطلبيات قيد الانتظار</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4">
                        <div class="panel panel-body">
                            <div class="media no-margin">
                                <div class="media-body">
                                    <h3 class="no-margin text-semibold">1000</h3>
                                    <span class="text-uppercase text-size-mini text-muted">كمية الوقود مباعة ( لتر )</span>
                                </div>

                                <div class="media-right media-middle">
                                    <i class="fa fa-tint icon-3x text-black"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                 
                </div>

                <div class="panel panel-flat">
                    <div class="panel-heading">
                          <h1 class="page-title">كمبة الوقود المباع ( الف لتر )</h1>
                        
                    </div>

                    <div class="panel-body">
                        <div class="chart-container">
                    {{--       <canvas id="myChart" width="400" height="400"></canvas> --}}
  <canvas id="myChart" width="400" height="100"></canvas>
<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["120", "110", "70", "130", "90", "92"],
        datasets: [{
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                 'rgba(255, 99, 132, 0.2)',
                 'rgba(255, 99, 132, 0.2)',
                 'rgba(255, 99, 132, 0.2)',
                  'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                  'rgba(255, 99, 132, 0.2)',
                 'rgba(255, 99, 132, 0.2)',
                 'rgba(255, 99, 132, 0.2)',
                 'rgba(255, 99, 132, 0.2)',
                 'rgba(255, 99, 132, 0.2)',
                 'rgba(255, 99, 132, 0.2)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>
                        </div>
                    </div>
                </div>

                </div>
@endif




@endsection