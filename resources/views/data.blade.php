<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>HWdata</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400italic,700%7CMontserrat:400,700" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">

    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
  </head>

  <body>

    <div id="h">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-md-6 centered" style="color: black;font-size: 25px;text-align: center">
            <h3>HW data</h3>
           <table style="width:100%" border="3">
  <tr>
    <th>id</th>
    <th>data</th> 

  </tr>

    @foreach($hwdata as $data)
      <tr>
    <td> {{ $data->id }}</td>
      <td> {{ $data->data }}</td>
       </tr>
  @endforeach
 

</table>
            
          </div>
           

        </div>
      </div>
    </div><!--/Header-->

       






    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
