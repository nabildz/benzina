<?php

namespace App\Http\Controllers;

use App\Order;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index($id)
    {

        return view('orders', ['orders' => Order::where($id)]);
    }
}