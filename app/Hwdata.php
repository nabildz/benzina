<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Spatial;

class Hwdata extends Model
{
    
      use Spatial;

      // $table='';
       protected $table = 'Hwdata';

      protected $fillable = [
        'id', 'data'
    	];
}
