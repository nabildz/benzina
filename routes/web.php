<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});


Route::get('/data/send/{data}', function ($data) {
	// $hwdata = App/hwdata::all();

		$hwdata = new App\Hwdata;


        $hwdata->data = $data;


       


        if ($data && $hwdata->save()) {
        	return response()->json([
    	'status' => 'OK',
		]);
        }else{
        	return response()->json([
    	'status' => 'fail',
		]);
        }

    	
});


Route::get('/data/view', function () {
	$hwdata = App\Hwdata::all();
    return view('data',compact('hwdata'));
});

// Route::get('admin/orders', function () {

	
//     return view('orders');
// });

// Route::get('admin/orders/pending', function () {
//     return view('orders_pending');
// });


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
